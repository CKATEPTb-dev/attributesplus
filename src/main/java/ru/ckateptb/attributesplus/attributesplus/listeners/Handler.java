package ru.ckateptb.attributesplus.attributesplus.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.ckateptb.attributesplus.attributesplus.configs.Settings;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.tableapi.menu.inventory.InventoryMenuBuilder;
import ru.ckateptb.tableapi.menu.inventory.ItemListener;

import java.util.ArrayList;
import java.util.List;

public class Handler implements Listener {

    @EventHandler
    public void on(PlayerInteractEvent e) {
        ItemStack item = e.getItem();
        if (!Attributed.isSocket(item)) return;
        ItemMeta meta = item.getItemMeta();
        Player player = e.getPlayer();
        List<ItemStack> items = new ArrayList<>();
        Inventory inventory = player.getInventory();
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack stack = inventory.getItem(i);
            if (Attributed.canApplySocket(stack)) {
                items.add(stack);
            }
        }
        if (items.isEmpty()) {
            player.sendMessage(Settings.hasNotSlot);
            return;
        }
        InventoryMenuBuilder builder = new InventoryMenuBuilder().withSize(9 * 4).withTitle("");
        for (int i = 0; i < items.size(); i++) {
            if (i > (9 * 4) - 1)
                return;
            builder.withItem(i, items.get(i), new ItemListener() {
                @Override
                public void onInteract(Player player, ClickType action, ItemStack it) {
                    if (player.getInventory().getItemInMainHand().equals(item)) {
                        int amount = item.getAmount();
                        if (amount == 1)
                            player.getInventory().removeItem(item);
                        else
                            item.setAmount(amount - 1);
                        ItemMeta m = it.getItemMeta();
                        List<String> lore = m.getLore();
                        lore.remove(Settings.socketSlot);
                        for (String line : meta.getLore()) {
                            if (Settings.socketLore.contains(line))
                                continue;
                            lore.add(line);
                        }
                        m.setLore(lore);
                        it.setItemMeta(m);
                    }
                    player.closeInventory();
                }
            }, ClickType.LEFT);
        }
        builder.show(player);
    }
}
