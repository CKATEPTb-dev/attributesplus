package ru.ckateptb.attributesplus.attributesplus.events;


import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CalculateAttributeEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();

    final String attribute;
    double amount;
    final LivingEntity entity;

    public CalculateAttributeEvent(String attribute, double amount, LivingEntity entity) {
        this.attribute = attribute;
        this.amount = amount;
        this.entity = entity;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    public String getAttribute() {
        return attribute;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LivingEntity getEntity() {
        return entity;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }
}
