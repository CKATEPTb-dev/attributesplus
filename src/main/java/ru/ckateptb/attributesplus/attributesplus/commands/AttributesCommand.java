package ru.ckateptb.attributesplus.attributesplus.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.ckateptb.attributesplus.attributesplus.AttributesPlus;
import ru.ckateptb.attributesplus.attributesplus.configs.Settings;
import ru.ckateptb.attributesplus.attributesplus.objects.Attribute;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.attributesplus.objects.IAttribute;
import ru.ckateptb.tableapi.commands.ACommand;

import java.util.ArrayList;
import java.util.List;

import static ru.ckateptb.attributesplus.attributesplus.AttributesPlus.PERMISSION_ATTRIBUTES;

public class AttributesCommand extends ACommand {
    public AttributesCommand() {
        super("att", "attributes");
    }

    @Override
    public boolean progress(CommandSender sender, String[] args) {
        if (!sender.hasPermission(PERMISSION_ATTRIBUTES))
            return false;
        switch (args.length) {
            case 0: {
                sendHelp(sender);
                return true;
            }
            case 1: {
                switch (args[0].toLowerCase()) {
                    case "hide": {
                        hideFlags(sender);
                        return true;
                    }
                    case "reload": {
                        AttributesPlus.getInstance().reload();
                        sender.sendMessage(Settings.reload);
                        return true;
                    }
                    case "socket": {
                        takeSocket(sender);
                        return true;
                    }
                }
            }
        }
        if (args.length > 1) {
            if (args[0].equalsIgnoreCase("name")) {
                setName(sender, args);
                return true;
            }
            if (args[0].equalsIgnoreCase("add")) {
                if (args[0].equalsIgnoreCase("add")) {
                    addAttribute(sender, args);
                    return true;
                }
            }
            if (args.length > 2) {
                if (args[0].equalsIgnoreCase("lore")) {
                    if (args[1].equalsIgnoreCase("add")) {
                        addLore(sender, args);
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("remove")) {
                        removeLore(sender, Integer.parseInt(args[2]));
                        return true;
                    }
                    if (args.length > 3) {
                        if (args[1].equalsIgnoreCase("set")) {
                            setLore(sender, args);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private void removeLore(CommandSender sender, int line) {
        if (!(sender instanceof Player))
            return;
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage(Settings.emptyHand);
            return;
        }
        if (!item.hasItemMeta())
            return;
        ItemMeta meta = item.getItemMeta();
        if (!meta.hasLore())
            return;
        List<String> lore = meta.getLore();
        lore.remove(line);
        meta.setLore(lore);
        item.setItemMeta(meta);
        player.getInventory().setItemInMainHand(item);
    }

    private void setLore(CommandSender sender, String... args) {
        if (!(sender instanceof Player))
            return;
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage(Settings.emptyHand);
            return;
        }
        ItemMeta meta;
        if (!item.hasItemMeta())
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        else
            meta = item.getItemMeta();
        List<String> lore;
        if (!meta.hasLore())
            lore = new ArrayList<>();
        else
            lore = meta.getLore();
        StringBuilder builder = new StringBuilder();
        for (int i = 3; i < args.length; i++) {
            builder.append(args[i]);
            if (i != args.length - 1)
                builder.append(" ");
        }
        int line = Integer.parseInt(args[2]);
        if (lore.size() < line)
            return;
        lore.set(--line, builder.toString().replaceAll("&", "§"));
        meta.setLore(lore);
        item.setItemMeta(meta);
        player.getInventory().setItemInMainHand(item);
    }

    private void addLore(CommandSender sender, String... args) {
        if (!(sender instanceof Player))
            return;
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage(Settings.emptyHand);
            return;
        }
        ItemMeta meta;
        if (!item.hasItemMeta())
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        else
            meta = item.getItemMeta();
        List<String> lore;
        if (!meta.hasLore())
            lore = new ArrayList<>();
        else
            lore = meta.getLore();
        StringBuilder name = new StringBuilder();
        for (int i = 2; i < args.length; i++) {
            name.append(args[i]);
            if (i != args.length - 1)
                name.append(" ");
        }
        lore.add(name.toString().replaceAll("&", "§"));
        meta.setLore(lore);
        item.setItemMeta(meta);
        player.getInventory().setItemInMainHand(item);
    }

    private void addAttribute(CommandSender sender, String... args) {
        if (!(sender instanceof Player))
            return;
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage(Settings.emptyHand);
            return;
        }
        Attribute attribute = Attribute.getByName(args[1]);
        if (attribute == null) {
            player.sendMessage(String.format(Settings.unKnown, args[1]));
            return;
        }
        ItemMeta meta;
        if (!item.hasItemMeta())
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        else
            meta = item.getItemMeta();
        if (!meta.hasLore())
            meta.setLore(new ArrayList<>());
        List<String> lore = meta.getLore();
        List<String> toRemove = new ArrayList<>();
        for (String line : lore) {
            if (line.contains(attribute.getDisplay()))
                toRemove.add(line);
        }
        lore.removeAll(toRemove);
        if (attribute.isVariable()) {
            if (args.length < 3)
                return;
            lore.add(attribute.getDisplay() + args[2]);
        } else
            lore.add(attribute.getDisplay());
        meta.setLore(lore);
        item.setItemMeta(meta);
        attribute.after(player, item);
    }

    private void takeSocket(CommandSender sender) {
        if (!(sender instanceof Player))
            return;
        Player player = (Player) sender;
        player.getInventory().addItem(Attributed.getSocket());
    }

    private void setName(CommandSender sender, String... args) {
        if (!(sender instanceof Player))
            return;
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage(Settings.emptyHand);
            return;
        }
        ItemMeta meta;
        if (!item.hasItemMeta())
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        else
            meta = item.getItemMeta();
        StringBuilder name = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            name.append(args[i]);
            if (i != args.length - 1)
                name.append(" ");
        }
        meta.setDisplayName(name.toString().replaceAll("&", "§"));
        item.setItemMeta(meta);
        player.getInventory().setItemInMainHand(item);
    }

    private void hideFlags(CommandSender sender) {
        if (!(sender instanceof Player))
            return;
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage(Settings.emptyHand);
            return;
        }
        ItemMeta meta;
        if (!item.hasItemMeta())
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        else
            meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        item.setItemMeta(meta);
        player.getInventory().setItemInMainHand(item);
    }

    private void sendHelp(CommandSender sender) {
        for (String line : Settings.attributesHelp)
            sender.sendMessage(line);
    }

    @Override
    public List<String> tab(CommandSender sender, String[] args, List<String> list) {
        List<String> tab = new ArrayList<>();
        if (args.length == 1)
            for (String s : new String[]{"add", "lore", "hide", "reload", "socket", "name"})
                if (s.toLowerCase().startsWith(args[0].toLowerCase()))
                    tab.add(s);
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("add")) {
                for (IAttribute attribute : Attribute.getAttributes()) {
                    String s = attribute.getName();
                    if (s.toLowerCase().startsWith(args[1].toLowerCase()))
                        tab.add(s);
                }
            }
            if (args[0].equalsIgnoreCase("lore")) {
                for (String s : new String[]{"add", "remove", "set"})
                    if (s.toLowerCase().startsWith(args[1].toLowerCase()))
                        tab.add(s);
            }
        }
        if (tab.isEmpty())
            tab = list;
        return tab;
    }
}
