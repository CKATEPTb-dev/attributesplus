package ru.ckateptb.attributesplus.attributesplus.configs;

import org.bukkit.Material;
import ru.ckateptb.attributesplus.attributesplus.AttributesPlus;
import ru.ckateptb.tableapi.configs.Configurable;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@Configurable.ConfigFile()
public class Settings extends Configurable {


    @Configurable.ConfigField(name = "calculateOffHandsAttributes")
    public static Boolean offHand = true;

    @Configurable.ConfigField(name = "language.reload")
    public static String reload = "§a§lConfiguration successfully rebooted";

    @Configurable.ConfigField(name = "language.attributes.unknown")
    public static String unKnown = "§c§lUnknown attribute §n%s";

    @Configurable.ConfigField(name = "language.emptyHand")
    public static String emptyHand = "§c§lTake the item in your hand!";

    @Configurable.ConfigField(name = "language.attributes.hasNotSlot")
    public static String hasNotSlot = "§c§lThere is no item in your inventory where you can insert a socket";

    @Configurable.ConfigField(name = "language.attributes.help")
    public static List<String> attributesHelp = Arrays.asList(
            "§6§l/att add <attribute> [value] - Add attribute",
            "§6§l/att lore add <line> - Add line to lore",
            "§6§l/att lore set <number> <line> - Set lore",
            "§6§l/att lore remove <number> - Remove line of lore",
            "§6§l/att name <name> - Re-name",
            "§6§l/att hide - Hide flags",
            "§6§l/att socket - Get an empty socket",
            "§6§l/att reload - Reload configuration");

    @Configurable.ConfigField(name = "attributes.socket.name")
    public static String socketName = "§6§lGain crystal";

    @Configurable.ConfigField(name = "attributes.socket.material")
    public static String socketMaterial = Material.EMERALD.toString();

    @Configurable.ConfigField(name = "attributes.socket.slot")
    public static String socketSlot = "§8§l<empty slot>";

    @Configurable.ConfigField(name = "attributes.socket.lore")
    public static List<String> socketLore = Arrays.asList("§7§lUse to power up an item.");

    public Settings() {
        super(AttributesPlus.getInstance().getDataFolder() + File.separator + "settings");
    }
}
