package ru.ckateptb.attributesplus.attributesplus;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.ckateptb.attributesplus.attributesplus.commands.AttributesCommand;
import ru.ckateptb.attributesplus.attributesplus.configs.Settings;
import ru.ckateptb.attributesplus.attributesplus.listeners.Handler;
import ru.ckateptb.attributesplus.attributesplus.objects.Attribute;
import ru.ckateptb.attributesplus.attributesplus.objects.IAddonAttribute;
import ru.ckateptb.attributesplus.attributesplus.objects.SocketAttribute;
import ru.ckateptb.tableapi.addons.AddonLoader;

import java.io.File;
import java.util.List;

public final class AttributesPlus extends JavaPlugin {

    public static final String PERMISSION_ATTRIBUTES = "AttributesPlus.use";

    private static AttributesPlus instance;
    private Settings config;
    private File attributeFolder;

    public static AttributesPlus getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getPluginManager().registerEvents(new Handler(), this);
        //==========================
        //        Attributes
        //==========================
        {
            new AttributesCommand();
            attributeFolder = new File(getDataFolder() + File.separator + "Attributes");
            if (attributeFolder.exists() || attributeFolder.mkdir())
                Bukkit.getScheduler().runTaskLater(this, () -> {
                    final AddonLoader<Attribute> attributeLoader = new AddonLoader<>(this, attributeFolder);
                    final List<Attribute> attributes = attributeLoader.load(Attribute.class, Attribute.class);
                    attributes.add(new SocketAttribute());
                    attributes.forEach(Attribute::enable);
                    reload();
                }, 1);
        }
    }

    @Override
    public void onDisable() {
    }

    public void reload() {
        this.config = new Settings();
        Attribute.getAttributes().forEach(IAddonAttribute::getConfig);
    }

    public Settings getMainConfig() {
        return config;
    }

    public File getAttributesFolder() {
        return attributeFolder;
    }
}
