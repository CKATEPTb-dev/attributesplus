package ru.ckateptb.attributesplus.attributesplus.objects;

import org.bukkit.event.Listener;
import ru.ckateptb.attributesplus.attributesplus.AttributesPlus;
import ru.ckateptb.attributesplus.attributesplus.configs.Settings;
import ru.ckateptb.tableapi.configs.Configurable;

public class SocketAttribute extends Attribute {
    @Override
    public String getRequiredPlugin() {
        return null;
    }

    @Override
    public Listener getListener() {
        return null;
    }

    @Override
    public Configurable getConfig() {
        return AttributesPlus.getInstance().getMainConfig();
    }

    @Override
    public String getName() {
        return "socket";
    }

    @Override
    public String getDisplay() {
        return Settings.socketSlot;
    }

    @Override
    public String getDescription() {
        return "Slot for Amplification Crystal";
    }

    @Override
    public Boolean isVariable() {
        return false;
    }
}
