package ru.ckateptb.attributesplus.attributesplus.objects;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface IAttribute {
    String getName();

    String getDisplay();

    String getDescription(); //todo

    Boolean isVariable();

    void after(Player player, ItemStack itemStack);
}
