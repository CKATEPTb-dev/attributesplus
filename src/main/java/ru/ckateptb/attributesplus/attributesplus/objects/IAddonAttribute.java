package ru.ckateptb.attributesplus.attributesplus.objects;

import org.bukkit.event.Listener;
import ru.ckateptb.tableapi.configs.Configurable;

/**
 * @author CKATEPTb
 */
public interface IAddonAttribute extends IAttribute {

    String getRequiredPlugin();

    Listener getListener();

    Configurable getConfig();
}
