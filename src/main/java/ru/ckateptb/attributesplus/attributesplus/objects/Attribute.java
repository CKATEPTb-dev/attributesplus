package ru.ckateptb.attributesplus.attributesplus.objects;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import ru.ckateptb.attributesplus.attributesplus.AttributesPlus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public abstract class Attribute implements IAddonAttribute {

    private static List<IAddonAttribute> attributes = new ArrayList<>();
    public final AttributesPlus api;
    public final Logger log;

    /**
     * The default constructor is needed to create a fake instance of each
     * Attribute via reflection. More specifically, Loader calls
     * getDeclaredConstructor which is only usable with a public default
     * constructor. Reflection lets us create a list of all of the plugin's
     * abilities when the plugin first loads.
     */
    public Attribute() {
        this.api = AttributesPlus.getInstance();
        this.log = this.api.getLogger();
    }

    public static List<IAddonAttribute> getAttributes() {
        return Collections.unmodifiableList(attributes);
    }

    public static Attribute getByName(String name) {
        for (IAttribute attribute : attributes) {
            if (attribute.getName().equalsIgnoreCase(name))
                return (Attribute) attribute;
        }
        return null;
    }

    public void enable() {
        String pl = getRequiredPlugin();
        if (pl == null) pl = AttributesPlus.getInstance().getName();
        Plugin plugin = Bukkit.getPluginManager().getPlugin(pl);
        if (plugin != null && plugin.isEnabled()) {
            if (getName() != null)
                attributes.add(this);
            Listener listener = getListener();
            if (listener != null)
                Bukkit.getPluginManager().registerEvents(listener, this.api);
            this.log.info(String.format("Attribute %s loaded!", getName()));
        } else {
            this.log.warning(String.format("Attribute %s not loaded, plugin %s is missing!", getName(), getRequiredPlugin()));
        }
    }

    @Override
    public void after(Player player, ItemStack item) {
        player.getInventory().setItemInMainHand(item);
    }
}
