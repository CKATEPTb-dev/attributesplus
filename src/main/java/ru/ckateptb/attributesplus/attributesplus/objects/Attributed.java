package ru.ckateptb.attributesplus.attributesplus.objects;

import net.minecraft.server.v1_12_R1.ItemArmor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.ckateptb.attributesplus.attributesplus.configs.Settings;
import ru.ckateptb.attributesplus.attributesplus.events.CalculateAttributeEvent;

import java.util.ArrayList;
import java.util.List;

public class Attributed {

    private LivingEntity entity;

    public Attributed(LivingEntity entity) {
        this.entity = entity;
    }

    public static boolean isSocket(ItemStack item) {
        if (item == null) return false;
        Material type = item.getType();
        ItemStack socket = Attributed.getSocket();
        if (type == null || type == Material.AIR || type != socket.getType() || !item.hasItemMeta()) return false;
        ItemMeta meta = item.getItemMeta();
        if (!meta.hasLore()) return false;
        ItemMeta socketMeta = socket.getItemMeta();
        return meta.getDisplayName().equals(socketMeta.getDisplayName()) && !meta.getLore().equals(socketMeta.getLore());
    }

    public static boolean isArmor(ItemStack stack) {
        net.minecraft.server.v1_12_R1.ItemStack nmsstack = CraftItemStack.asNMSCopy(stack);
        return nmsstack.getItem() instanceof ItemArmor;
    }

    public static ItemStack getSocket() {
        ItemStack item = new ItemStack(Material.valueOf(Settings.socketMaterial));
        ItemMeta meta = item.hasItemMeta() ? item.getItemMeta() : Bukkit.getItemFactory().getItemMeta(Material.valueOf(Settings.socketMaterial));
        meta.setDisplayName(Settings.socketName);
        meta.setLore(Settings.socketLore);
        item.setItemMeta(meta);
        return item;
    }

    public static boolean canApplySocket(ItemStack item) {
        if (item != null && item.hasItemMeta()) {
            ItemMeta meta = item.getItemMeta();
            if (!meta.hasLore())
                meta.setLore(new ArrayList<>());
            List<String> lore = meta.getLore();
            for (String line : lore) {
                if (line.contains(Settings.socketSlot))
                    return true;
            }
        }
        return false;
    }

    public Double get(String attribute) {
        double amount = 0;
        for (ItemStack item : getEquip()) {
            if (item == null || !item.hasItemMeta())
                continue;
            ItemMeta meta = item.getItemMeta();
            if (!meta.hasLore())
                meta.setLore(new ArrayList<>());
            List<String> lore = meta.getLore();
            for (String line : lore) {
                if (line.contains(attribute))
                    amount += Double.parseDouble(line.replace(attribute, ""));
            }
        }

        CalculateAttributeEvent event = new CalculateAttributeEvent(attribute, amount, entity);
        Bukkit.getServer().getPluginManager().callEvent(event);
        return event.getAmount();
    }

    private ItemStack[] getEquip() {
        EntityEquipment equip = entity.getEquipment();
        ItemStack head = equip.getHelmet();
        ItemStack chest = equip.getChestplate();
        ItemStack legs = equip.getLeggings();
        ItemStack boots = equip.getBoots();
        ItemStack offHand = Settings.offHand ? equip.getItemInOffHand() : null;
        if (offHand != null && (isArmor(offHand) || isSocket(offHand)))
            offHand = null;
        ItemStack hand = equip.getItemInMainHand();
        if (hand != null && (isArmor(hand) || isSocket(hand)))
            hand = null;
        return new ItemStack[]{head, chest, legs, boots, offHand, hand};
    }
}
